package com.starbuzz.coffee.condiment;

import com.starbuzz.coffee.beverage.Beverage;

import java.math.BigDecimal;

public class Milk extends CondimentDecorator {
    public Milk(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public BigDecimal cost() {
        return new BigDecimal("123.2").add(this.beverage.cost());
    }

    @Override
    public String getDescription() {
        return this.beverage.getDescription() +  ", Milk";
    }
}
