package com.starbuzz.coffee.condiment;

import com.starbuzz.coffee.beverage.Beverage;

import java.math.BigDecimal;

public class Mocha extends CondimentDecorator {
    public Mocha(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return this.beverage.getDescription() + ", Mocha";
    }

    @Override
    public BigDecimal cost() {
        return new BigDecimal("99.9").add(this.beverage.cost());
    }
}
