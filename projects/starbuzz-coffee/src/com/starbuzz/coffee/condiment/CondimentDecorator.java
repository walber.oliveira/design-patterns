package com.starbuzz.coffee.condiment;

import com.starbuzz.coffee.beverage.Beverage;

public abstract class CondimentDecorator extends Beverage {
    protected Beverage beverage;

    public abstract String getDescription();
}
