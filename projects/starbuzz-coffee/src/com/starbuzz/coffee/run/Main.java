package com.starbuzz.coffee.run;

import com.starbuzz.coffee.beverage.Beverage;
import com.starbuzz.coffee.beverage.DarkRoast;
import com.starbuzz.coffee.beverage.Expresso;
import com.starbuzz.coffee.condiment.Milk;
import com.starbuzz.coffee.condiment.Mocha;

public class Main {

    public static void main(String[] args) {
        Beverage beverage = new Expresso();
        System.out.println(beverage.getDescription() + " R$ " + beverage.cost());

        Beverage beverage1 = new DarkRoast();
        beverage1 = new Milk(beverage1);
        beverage1 = new Mocha(beverage1);

        System.out.println(beverage1.getDescription() + " R$ " + beverage1.cost());
    }
}
