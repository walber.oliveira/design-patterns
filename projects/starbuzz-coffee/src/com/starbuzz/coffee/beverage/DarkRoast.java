package com.starbuzz.coffee.beverage;

import java.math.BigDecimal;

public class DarkRoast extends Beverage {

    public DarkRoast() {
        this.description = "Dark Roast";
    }

    @Override
    public BigDecimal cost() {
        return new BigDecimal("9.99");
    }
}
