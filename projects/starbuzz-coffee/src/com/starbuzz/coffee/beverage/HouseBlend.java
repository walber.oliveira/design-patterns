package com.starbuzz.coffee.beverage;

import java.math.BigDecimal;

public class HouseBlend extends Beverage {
    @Override
    public BigDecimal cost() {
        return new BigDecimal("1.52");
    }
}
