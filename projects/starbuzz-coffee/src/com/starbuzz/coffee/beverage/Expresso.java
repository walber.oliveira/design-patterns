package com.starbuzz.coffee.beverage;

import java.math.BigDecimal;

public class Expresso extends Beverage {

    public Expresso() {
        this.description = "Expresso";
    }

    @Override
    public BigDecimal cost() {
        return new BigDecimal("1.99");
    }
}
