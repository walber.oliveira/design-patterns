package com.objectville.remote;

public interface Command {
    void execute();
}
