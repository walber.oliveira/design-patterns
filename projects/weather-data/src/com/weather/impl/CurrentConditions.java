package com.weather.impl;

import com.weather.observer.DisplayElement;
import com.weather.observer.Observer;
import com.weather.observer.Subject;

import java.math.BigDecimal;

public class CurrentConditions implements Observer, DisplayElement {

    private final Subject weatherData;
    private BigDecimal temperature;
    private BigDecimal humidity;
    private BigDecimal pressure;

    public CurrentConditions(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Condicoes atuais: " + temperature + " C e " + humidity + " % humidade.");
    }

    @Override
    public void update(BigDecimal temperature, BigDecimal humidity, BigDecimal pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        display();
    }
}
