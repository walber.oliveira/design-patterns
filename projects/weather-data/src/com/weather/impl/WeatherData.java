package com.weather.impl;

import com.weather.observer.Observer;
import com.weather.observer.Subject;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class WeatherData implements Subject {
    // Declarações de varável de instância
    private Set<Observer> observers;
    private BigDecimal temperature;
    private BigDecimal humidity;
    private BigDecimal pressure;

    public WeatherData() {
        this.observers = new HashSet<Observer>();
    }

    public void measurementsChanged() {
        notifyObservers();
    }

    public void setMeasurementsChanged(BigDecimal temperature, BigDecimal humidity, BigDecimal pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    @Override
    public void registerObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(observer -> observer.update(temperature, humidity, pressure));
    }

    // Outros métodos WeatherData aqui

}
