package com.weather.run;

import com.weather.display.StatisticsDisplay;
import com.weather.impl.CurrentConditions;
import com.weather.impl.WeatherData;

import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        CurrentConditions currentConditions = new CurrentConditions(weatherData);
        StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);

        BigDecimal temp1 = new BigDecimal("25.5");
        BigDecimal hum1 = new BigDecimal("5");
        BigDecimal press1 = new BigDecimal("15.2");
        BigDecimal temp2 = new BigDecimal("35.5");
        BigDecimal hum2 = new BigDecimal("10");
        BigDecimal press2 = new BigDecimal("29.2");
        BigDecimal temp3 = new BigDecimal("70.5");
        BigDecimal hum3 = new BigDecimal("20");
        BigDecimal press3 = new BigDecimal("59.2");
        weatherData.setMeasurementsChanged(temp1, hum1, press1);
//        weatherData.setMeasurementsChanged(temp2, hum2, press2);
//        weatherData.setMeasurementsChanged(temp3, hum3, press3);

    }
}
