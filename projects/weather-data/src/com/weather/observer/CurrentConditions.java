package com.weather.observer;

import java.math.BigDecimal;

public class CurrentConditions implements Observer, DisplayElement {
    @Override
    public void display() {

    }

    @Override
    public void update(BigDecimal temperature, BigDecimal humidity, BigDecimal pressure) {

    }
}
