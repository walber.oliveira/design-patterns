package com.weather.observer;

import java.math.BigDecimal;

public interface Observer {

    public void update(BigDecimal temperature, BigDecimal humidity, BigDecimal pressure);
}
