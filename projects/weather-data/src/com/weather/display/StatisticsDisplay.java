package com.weather.display;

import com.weather.observer.Observer;
import com.weather.observer.Subject;

import java.math.BigDecimal;

public class StatisticsDisplay implements DisplayElement, Observer {
    private Subject weatherData;
    private BigDecimal temperature;
    private BigDecimal humidity;
    private BigDecimal pressure;

    public StatisticsDisplay(Subject weatherData) {
        this.weatherData = weatherData;
//        weatherData.registerObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Estatisticas: " + temperature + " C e " + humidity + " % humidade.");
    }

    @Override
    public void update(BigDecimal temperature, BigDecimal humidity, BigDecimal pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        display();
    }
}
