package com.weather.display;

public interface DisplayElement {

    public void display();
}
