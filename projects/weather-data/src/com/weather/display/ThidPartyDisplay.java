package com.weather.display;

import com.weather.observer.Observer;

import java.math.BigDecimal;

public class ThidPartyDisplay implements DisplayElement, Observer {
    @Override
    public void display() {

    }

    @Override
    public void update(BigDecimal temperature, BigDecimal humidity, BigDecimal pressure) {

    }
}
