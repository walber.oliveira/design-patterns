package com.duck.run;

import com.duck.game.Duck;
import com.duck.game.MallardDuck;
import com.duck.game.RubberDuck;
import com.duck.game.fly.FlyWithWings;

public class DuckGameRun {

    public static void main(String[] args) {
        Duck duck = new RubberDuck();
        duck.display();
        duck.performFly();
        duck.performQuack();
        duck.swim();
    }
}
