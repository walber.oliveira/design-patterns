package com.duck.game;

import com.duck.game.fly.FlyNoWay;
import com.duck.game.quack.MuteQuack;

public class RubberDuck extends Duck {
    public RubberDuck() {
        this.setFlyBehavior(new FlyNoWay());
        this.setQuackBehavior(new MuteQuack());
    }

    @Override
    public void display() {
        System.out.println("Rubber Duck????");
    }
}
