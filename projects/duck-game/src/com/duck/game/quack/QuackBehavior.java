package com.duck.game.quack;

public interface QuackBehavior {
    public void quack();
}
