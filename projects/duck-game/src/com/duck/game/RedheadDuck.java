package com.duck.game;

import com.duck.game.fly.FlyWithWings;
import com.duck.game.quack.Squeak;

public class RedheadDuck extends Duck {

    public RedheadDuck() {
        this.setFlyBehavior(new FlyWithWings());
        this.setQuackBehavior(new Squeak());
    }

    @Override
    public void display() {
        System.out.println("I'm a REDDDDD DUCKKKKK");
    }
}
