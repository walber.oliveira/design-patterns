package com.duck.game.fly;

public interface FlyBehavior {
    public void fly();
}
