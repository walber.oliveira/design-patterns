package com.objectville.pizza;

public class Main {

    public static void main(String[] args) {
        Pizza pizza = orderPizza("chesse");
    }

    private static Pizza orderPizza(String type) {
        Pizza pizza = null;
        if(type.equals("chesse")) {
            pizza = new ChessePizza();
        } else if (type.equals("greek")) {
            pizza = new GreekPizza();
        }
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
